import React from 'react';
import CreateTaskControl from './CreateTaskControl';
import Task from './Task';

class Content extends React.Component {
  constructor(props) {
    super(props);
    this.state = { tasks: [] };
    this.updateState = this.updateState.bind(this);
    this.clickHandlerDelete = this.clickHandlerDelete.bind(this);
  }

  updateState(val) {
    console.log(val);
    this.setState((prevState) => {
      //console.log(value);
      return {tasks: prevState.tasks.concat(val)}
    });
  }

  renderTasks() {
    const taskList = this.state.tasks.map((task) => {
      return <Task callback={this.clickHandlerDelete} key={task} text={task} />;
    });

    return taskList;
  }

  clickHandlerDelete(val) {
    this.setState((prevState) => {
      var arr = [].concat(prevState.tasks);
      var index = arr.indexOf(val);
      if (index >= 0) {
        arr.splice( index, 1 );
      }
      return {tasks: arr}
    });

  }

  render() {
    return (
      <div id="content">
        <CreateTaskControl callback={(val) => this.updateState(val)} />
        {this.renderTasks()}
      </div>
    );
  }
}

export default Content;
