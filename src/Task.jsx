import React from 'react';

class Task extends React.Component {

  handleClick() {
    this.props.callback(this.props.text);
    console.log('handleClock', this.props.text);
  }

  render() {
    return (
      <div className="task-item">
        <input className="inputTask" type="text" value={this.props.text} disabled />
        <button className="deleteButton" onClick={this.handleClick.bind(this)}>Delete</button>
      </div>
    );
  }
}

export default Task;
