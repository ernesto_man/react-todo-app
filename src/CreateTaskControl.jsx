import React from 'react';

class CreateTaskControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
  }

  handleChange(event) {
    this.setState({value: event.target.value});

  }

  clickHandler(){
    this.props.callback(this.state.value);
  }

  render() {
    const value = this.state.value;
    return (
      <div className="create-task-control">
        <input className="inputTask" placeholder="Write a task here" type="text" onChange={this.handleChange.bind(this)} />
        <button className="createButton" onClick={this.clickHandler.bind(this)}>Create</button>
      </div>
    );
  }
}

export default CreateTaskControl;
